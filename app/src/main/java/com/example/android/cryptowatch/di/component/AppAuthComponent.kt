package com.example.android.cryptowatch.di.component

import android.app.Application
import android.content.Context
import com.example.android.cryptowatch.di.AppModule
import com.example.android.cryptowatch.di.authenticationModule.AuthModelModule
import com.example.android.cryptowatch.di.ContextModule
import com.example.android.cryptowatch.di.coinModule.CoinModelModule
import com.example.android.cryptowatch.ui.App
import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentModel
import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, ContextModule::class, AuthModelModule::class])
interface AppAuthComponent {

    fun inject(app: App)

    fun getContext(): Context
    fun getFindAuthModel() : AuthenticationFragmentModel
    fun getApplication(): Application

}