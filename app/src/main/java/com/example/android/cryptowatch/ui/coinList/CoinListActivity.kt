package com.example.android.cryptowatch.ui.coinList

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentView
import com.google.android.gms.auth.api.signin.GoogleSignInAccount


class CoinListActivity : AppCompatActivity() {

    var GOOGLE_ACCOUNT = "google_account"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.android.cryptowatch.R.layout.activity_coin_list)


        val googleSignInAccount = intent.getParcelableExtra<GoogleSignInAccount>(GOOGLE_ACCOUNT)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(
                    com.example.android.cryptowatch.R.id.fragmentCoinListContainer,
                    CoinListFragmentView.newInstance(googleSignInAccount),
                    "coinFragment"
                )
                .commit()

        }
    }
}