package com.example.android.cryptowatch.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.android.cryptowatch.ui.authentication.AuthenticationActivity
import com.example.android.cryptowatch.ui.coinList.CoinListActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions


class SplashMainFragment : Fragment() {

    companion object {
        fun newInstance(): SplashMainFragment {
            return SplashMainFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.example.android.cryptowatch.R.layout.fragment_splash, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scheduleSplashScreen()
    }

    private fun scheduleSplashScreen() {
        val splashScreenDuration = getSplashScreenDuration()
        Handler().postDelayed(
                {
                    val alreadyLoggedAccount = GoogleSignIn.getLastSignedInAccount(context)
                    if (alreadyLoggedAccount != null) {
                        onLoggedIn(alreadyLoggedAccount)
                    } else {
                        routeToAppropriatePage()
                    }
                },
                splashScreenDuration
        )
    }

    private fun onLoggedIn(alreadyLoggedAccount: GoogleSignInAccount) {
        val intent = Intent(context, CoinListActivity::class.java)
        intent.putExtra(CoinListActivity().GOOGLE_ACCOUNT, alreadyLoggedAccount)
        startActivity(intent)
        activity!!.finish()
    }

    private fun routeToAppropriatePage() {
        val authIntent = Intent(context, AuthenticationActivity::class.java)
        startActivity(authIntent)
        activity!!.finish()
    }

    private fun getSplashScreenDuration() = 2000L
}