package com.example.android.cryptowatch.di.coinModule

import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentModel
import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentPresenter
import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentView
import dagger.Module
import dagger.Provides

@Module
class CoinMvpModule(private val view: CoinListFragmentView) {

    @Provides
    fun provideCoinView(): CoinListFragmentView {
        return view
    }

    @Provides
    fun provideCoinPresenter(
        view: CoinListFragmentView,
        model: CoinListFragmentModel
    ): CoinListFragmentPresenter {
        return CoinListFragmentPresenter(view, model)
    }
}