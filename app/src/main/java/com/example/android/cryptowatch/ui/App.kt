package com.example.android.cryptowatch.ui

import android.app.Application
import android.content.Context
import com.example.android.cryptowatch.di.AppModule
import com.example.android.cryptowatch.di.ContextModule
import com.example.android.cryptowatch.di.authenticationModule.AuthModelModule
import com.example.android.cryptowatch.di.coinModule.CoinModelModule
import com.example.android.cryptowatch.di.component.AppAuthComponent
import com.example.android.cryptowatch.di.component.AppCoinComponent
import com.example.android.cryptowatch.di.component.DaggerAppAuthComponent
import com.example.android.cryptowatch.di.component.DaggerAppCoinComponent


class App : Application() {

    private var authComponent: AppAuthComponent? = null
    private var coinComponent: AppCoinComponent? = null

    override fun onCreate() {
        super.onCreate()



        authComponent = DaggerAppAuthComponent.builder()
            .appModule(AppModule(this))
            .contextModule(ContextModule(this))
            .authModelModule(AuthModelModule())
            .build()

        coinComponent = DaggerAppCoinComponent.builder()
            .appModule(AppModule(this))
            .contextModule(ContextModule(this))
            .coinModelModule(CoinModelModule())
            .build()
    }

    fun getCoinComponent(): AppCoinComponent? {
        return coinComponent
    }

    fun getAuthComponent(): AppAuthComponent? {
        return authComponent
    }

    companion object {

        operator fun get(context: Context): App {
            return context.applicationContext as App
        }
    }
}