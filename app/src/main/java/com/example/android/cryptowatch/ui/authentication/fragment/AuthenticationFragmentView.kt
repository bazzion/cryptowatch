package com.example.android.cryptowatch.ui.authentication.fragment

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.android.cryptowatch.di.authenticationModule.AuthMvpModule
import com.example.android.cryptowatch.di.component.DaggerAuthFragmentComponent
import com.example.android.cryptowatch.ui.App
import com.example.android.cryptowatch.ui.authentication.fragment.authContract.IAuthView
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.fragment_authentication.view.*
import javax.inject.Inject


class AuthenticationFragmentView : Fragment(),
    IAuthView {


    @Inject
    @InjectPresenter
    lateinit var authenticationFragmentPresenter: AuthenticationFragmentPresenter
    @Inject
    lateinit var mContext: Context


    companion object {
        fun newInstance(): AuthenticationFragmentView {
            return AuthenticationFragmentView()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
            com.example.android.cryptowatch.R.layout.fragment_authentication,
            container,
            false
        )
        view.signInButton.setOnClickListener { authenticationFragmentPresenter.onGoogleButtonClick(this.activity!!) }
        return view
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerAuthFragmentComponent.builder().appAuthComponent(App[context?.applicationContext!!].getAuthComponent())
            .authMvpModule(AuthMvpModule(this))
            .build()
            .inject(this)
    }

    override fun startSignActivity(intent: Intent) {
        startActivityForResult(intent, AuthenticationFragmentModel().RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            AuthenticationFragmentModel().RC_SIGN_IN -> try {
                authenticationFragmentPresenter.openCryptoListActivity(data!!, mContext)
            } catch (e: ApiException) {
                Log.w("tag", "signInResult:failed code=" + e.statusCode)
            }
        }
    }

    override fun startCoinListActivity(intent: Intent) {
        startSignActivity(intent)
        activity!!.finish()
    }
}
