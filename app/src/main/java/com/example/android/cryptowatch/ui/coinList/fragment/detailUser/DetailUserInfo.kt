package com.example.android.cryptowatch.ui.coinList.fragment.detailUser

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.android.cryptowatch.ui.authentication.AuthenticationActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.android.synthetic.main.fragment_coin_list.view.*
import kotlinx.android.synthetic.main.fragment_user_deatil.*
import kotlinx.android.synthetic.main.fragment_user_deatil.view.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@SuppressLint("ValidFragment")
class DetailUserInfo(bundle: Bundle) : Fragment() {


    companion object {
        fun newInstance(google: GoogleSignInAccount): DetailUserInfo {
            val bundle = Bundle()
            bundle.putParcelable("key", google)
            return DetailUserInfo(bundle)
        }
    }

    var googleSignInAccount: GoogleSignInAccount = bundle.getParcelable("key")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
            com.example.android.cryptowatch.R.layout.fragment_user_deatil,
            container,
            false
        )


        Glide.with(view)
                .load(googleSignInAccount.photoUrl)
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(RequestOptions().circleCrop())
                .into(view.detailUserImage)
        view.detailUserName.text = googleSignInAccount.displayName
        view.detailUserEmail.text = googleSignInAccount.email

        view.logoutUserButton.setOnClickListener { startLogout() }

        return view
    }

    private fun startLogout() {
        val googleClient: GoogleSignInClient =
            GoogleSignIn.getClient(this.context!!, GoogleSignInOptions.DEFAULT_SIGN_IN)
        googleClient.signOut()
        val authIntent = Intent(context, AuthenticationActivity::class.java)
        startActivity(authIntent)
        activity!!.finish()
    }
}