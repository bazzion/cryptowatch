package com.example.android.cryptowatch.ui.authentication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.cryptowatch.R
import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentView

class AuthenticationActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.fragmentAuthenticationContainer, AuthenticationFragmentView.newInstance(),
                            "authFragment")
                    .commit()

        }
    }
}