package com.example.android.cryptowatch.di

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.example.android.cryptowatch.ui.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return app
    }

    @Provides
    @Singleton
    fun connectivity(): ConnectivityManager {
        return app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }
}