package com.example.android.cryptowatch.ui.authentication.fragment.authContract

import android.content.Intent
import com.arellomobile.mvp.MvpView

interface IAuthView: MvpView {

    fun startSignActivity(intent: Intent)
    fun startCoinListActivity(intent: Intent)
}