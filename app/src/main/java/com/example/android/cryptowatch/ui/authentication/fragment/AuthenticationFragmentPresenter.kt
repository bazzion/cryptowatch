package com.example.android.cryptowatch.ui.authentication.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.android.cryptowatch.ui.authentication.fragment.authContract.IAuthPresenter
import com.example.android.cryptowatch.ui.authentication.fragment.authContract.IAuthView
import com.example.android.cryptowatch.ui.coinList.CoinListActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException

@InjectViewState
class AuthenticationFragmentPresenter(
    private var view: AuthenticationFragmentView,
    private val model: AuthenticationFragmentModel
) : IAuthPresenter,
    MvpPresenter<IAuthView>() {

    private lateinit var mGoogleSignInClient: GoogleSignInClient


    override fun openCryptoListActivity(intent: Intent, context: Context) {
        val task = GoogleSignIn.getSignedInAccountFromIntent(intent)
        val account = task.getResult(ApiException::class.java)
        val coinListIntent = Intent(context, CoinListActivity::class.java)
        coinListIntent.putExtra(CoinListActivity().GOOGLE_ACCOUNT, account)
        view.startCoinListActivity(coinListIntent)
    }

    override fun onGoogleButtonClick(activity: Activity) {
        val signInOptions = model.getSignInOptions()
        mGoogleSignInClient = GoogleSignIn.getClient(activity, signInOptions)
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        view.startSignActivity(signInIntent)
    }
}
