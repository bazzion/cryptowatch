package com.example.android.cryptowatch.di.coinModule

import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentModel
import dagger.Module
import dagger.Provides

@Module
class CoinModelModule {

    @Provides
    fun provideCoinModelClass(): CoinListFragmentModel {
        return CoinListFragmentModel()
    }
}