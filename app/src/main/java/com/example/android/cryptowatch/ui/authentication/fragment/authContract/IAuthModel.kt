package com.example.android.cryptowatch.ui.authentication.fragment.authContract

import com.google.android.gms.auth.api.signin.GoogleSignInOptions

interface IAuthModel {

    fun getSignInOptions(): GoogleSignInOptions
}