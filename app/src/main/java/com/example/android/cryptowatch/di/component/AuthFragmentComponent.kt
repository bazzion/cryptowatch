package com.example.android.cryptowatch.di.component

import com.example.android.cryptowatch.di.authenticationModule.AuthMvpModule
import com.example.android.cryptowatch.di.scope.FragmentScope
import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentPresenter
import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentView
import dagger.Component


@FragmentScope
@Component(dependencies = [AppAuthComponent::class], modules = [AuthMvpModule::class])
interface AuthFragmentComponent {
    fun inject(authenticationFragmentView: AuthenticationFragmentView)

    fun getAuthPresenter(): AuthenticationFragmentPresenter
}