package com.example.android.cryptowatch.ui.authentication.fragment.authContract

import android.app.Activity
import android.content.Context
import android.content.Intent

interface IAuthPresenter {

    fun openCryptoListActivity(intent: Intent, context: Context)
    fun onGoogleButtonClick(activity: Activity)
}