package com.example.android.cryptowatch.di.authenticationModule

import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentModel
import dagger.Module
import dagger.Provides


@Module
class AuthModelModule {

    @Provides
    fun provideModelClass(): AuthenticationFragmentModel {
        return AuthenticationFragmentModel()
    }
}