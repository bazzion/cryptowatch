package com.example.android.cryptowatch.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "cryptoCoinDataBase")
data class CryptoCoinDB(

    @PrimaryKey val id: Int,
    @ColumnInfo(name = "logo") val logo: String,
    @ColumnInfo(name = "full_name") val fullName: String,
    @ColumnInfo(name = "short_name") val shortName: String,
    @ColumnInfo(name = "price") val price: Double,
    @ColumnInfo(name = "volume") val volume: Double,
    @ColumnInfo(name = "changes") val changes: Double,
    @ColumnInfo(name = "is_favourite") var isFavourite: Boolean = false
) {

    companion object {
        fun create(
            data: CryptoCoinCurrencies.Data,
            detail: CryptoCoinCurrencyInfo.Detail?
        ): CryptoCoinDB =
            CryptoCoinDB(
                data.id,
                detail?.logo ?: "",
                data.name,
                data.symbol,
                data.quote.USD.price,
                data.quote.USD.volume_24h,
                data.quote.USD.percent_change_24h
            )
    }
}