package com.example.android.cryptowatch.di.network

import com.example.android.cryptowatch.data.CryptoCoinCurrencies
import com.example.android.cryptowatch.data.CryptoCoinCurrencyInfo
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface CoinMarketCapApi {

    @GET("v1/cryptocurrency/listings/latest")
    @Headers("X-CMC_PRO_API_KEY: 864a20d3-585b-4953-8d55-057190475463")
    fun getCryptoCoinCurrencies(
        @Query("start") start: Int,
        @Query("limit") limit: Int,
        @Query("convert") convert: String = "USD"
    ): Observable<CryptoCoinCurrencies>

    @GET("v1/cryptocurrency/info")
    @Headers("X-CMC_PRO_API_KEY: 864a20d3-585b-4953-8d55-057190475463   ")
    fun getCryptoCoinCurrencyInfo(@Query("id") id: String): Observable<CryptoCoinCurrencyInfo>

}