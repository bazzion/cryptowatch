package com.example.android.cryptowatch.di.component

import com.example.android.cryptowatch.di.coinModule.CoinMvpModule
import com.example.android.cryptowatch.di.scope.FragmentScope
import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentPresenter
import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentView
import dagger.Component


@FragmentScope
@Component(dependencies = [AppCoinComponent::class], modules = [CoinMvpModule::class])
interface CoilListFragmentComponent {
    fun inject(coinListFragmentView: CoinListFragmentView)

    fun getCoinListPresenter(): CoinListFragmentPresenter
}