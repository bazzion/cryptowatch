package com.example.android.cryptowatch.ui.coinList.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.android.cryptowatch.R
import com.example.android.cryptowatch.di.coinModule.CoinMvpModule
import com.example.android.cryptowatch.di.component.DaggerCoilListFragmentComponent
import com.example.android.cryptowatch.ui.App
import com.example.android.cryptowatch.ui.coinList.fragment.detailUser.DetailUserInfo
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import kotlinx.android.synthetic.main.fragment_coin_list.view.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@SuppressLint("ValidFragment")
class CoinListFragmentView(bundle: Bundle) : Fragment() {


    companion object {
        fun newInstance(google: GoogleSignInAccount): CoinListFragmentView {
            val bundle = Bundle()
            bundle.putParcelable("key", google)
            return CoinListFragmentView(bundle)
        }
    }

    var googleSignInAccount: GoogleSignInAccount = bundle.getParcelable("key")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(
            R.layout.fragment_coin_list,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.userNameTextView.text = (googleSignInAccount.displayName).toString()
        Glide.with(view)
            .load(googleSignInAccount.photoUrl)
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
            .apply(RequestOptions().circleCrop())
            .into(view.userAvatarImage)
        view.userAvatarImage.setOnClickListener { openUserDetailFragment(googleSignInAccount) }
    }

    private fun openUserDetailFragment(account: GoogleSignInAccount) {
        fragmentManager!!.beginTransaction().replace(
            com.example.android.cryptowatch.R.id.fragmentCoinListContainer,
            DetailUserInfo.newInstance(account)
        ).addToBackStack(null)
            .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerCoilListFragmentComponent.builder()
            .appCoinComponent(App[context?.applicationContext!!].getCoinComponent())
            .coinMvpModule(CoinMvpModule(this))
            .build()
            .inject(this)
    }

}