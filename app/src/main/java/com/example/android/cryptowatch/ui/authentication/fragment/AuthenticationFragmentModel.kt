package com.example.android.cryptowatch.ui.authentication.fragment

import com.example.android.cryptowatch.ui.authentication.fragment.authContract.IAuthModel
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient


class AuthenticationFragmentModel : IAuthModel, GoogleApiClient.OnConnectionFailedListener {

    val RC_SIGN_IN = 2
    private val ID_TOKEN = "164861652593-ore24sv8caqa0qjhmlv7m7qesddkm6i2.apps.googleusercontent.com"
    lateinit var mGoogleSignInOptions: GoogleSignInOptions

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun getSignInOptions(): GoogleSignInOptions {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(ID_TOKEN)
            .requestEmail()
            .build()
        return mGoogleSignInOptions
    }
}
