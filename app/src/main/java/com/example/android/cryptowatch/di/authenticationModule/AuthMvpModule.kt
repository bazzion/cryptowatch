package com.example.android.cryptowatch.di.authenticationModule

import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentModel
import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentPresenter
import com.example.android.cryptowatch.ui.authentication.fragment.AuthenticationFragmentView
import dagger.Module
import dagger.Provides


@Module
 class AuthMvpModule(private val viewCallBack: AuthenticationFragmentView) {

    @Provides
    fun provideView(): AuthenticationFragmentView {
        return viewCallBack
    }

    @Provides
    fun providePresenter(
        view: AuthenticationFragmentView,
        model: AuthenticationFragmentModel
    ): AuthenticationFragmentPresenter {
        return AuthenticationFragmentPresenter(view, model)
    }
}