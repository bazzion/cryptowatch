package com.example.android.cryptowatch.di.component

import android.app.Application
import android.content.Context
import com.example.android.cryptowatch.di.AppModule
import com.example.android.cryptowatch.di.ContextModule
import com.example.android.cryptowatch.di.coinModule.CoinModelModule
import com.example.android.cryptowatch.ui.App
import com.example.android.cryptowatch.ui.coinList.fragment.CoinListFragmentModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ContextModule::class, CoinModelModule::class])
interface AppCoinComponent {

    fun inject(app: App)

    fun getContext(): Context
    fun getFindCoinModel(): CoinListFragmentModel
    fun getApplication(): Application

}