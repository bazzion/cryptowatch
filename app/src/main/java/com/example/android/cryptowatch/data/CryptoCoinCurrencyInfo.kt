package com.example.android.cryptowatch.data

class CryptoCoinCurrencyInfo(
    val data: Map<Int, Detail>,
    val status: Status
) {
    data class Detail(
        val category: String,
        val date_added: String,
        val id: Int,
        val logo: String,
        val name: String,
        val platform: Any?,
        val slug: String,
        val symbol: String,
        val tags: List<String>,
        val urls: Urls
    ) {
        data class Urls(
            val announcement: List<Any>,
            val chat: List<Any>,
            val explorer: List<String>,
            val message_board: List<String>,
            val reddit: List<String>,
            val source_code: List<String>,
            val twitter: List<Any>,
            val website: List<String>
        )
    }

    data class Status(
        val credit_count: Int,
        val elapsed: Int,
        val error_code: Int,
        val error_message: Any?,
        val timestamp: String
    )
}