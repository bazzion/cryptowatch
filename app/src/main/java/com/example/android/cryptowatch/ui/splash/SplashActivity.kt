package com.example.android.cryptowatch.ui.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.cryptowatch.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentSplashContainer, SplashMainFragment.newInstance(), "splashFragment")
                .commit()

        }
    }
}
